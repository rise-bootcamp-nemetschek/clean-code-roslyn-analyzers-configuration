# Airlines Project - Software Developer Intern Program

## Overview

The Airlines Project is a comprehensive training initiative for software developer interns, focusing on building a robust application centered around airport, airline, and flight management. This project encompasses a wide range of software development skills from basic programming to advanced web development and database integration.

## Rules of engagement
During entire bootcamp participiants has to comply to the following rules after the lecture of the according topic:
- Commit often - everytime when possible - when have a small part of the task running. Mandatory at the end of the day even if the code is not complete and do not build successfully
- Take care about architecture
- Write unit tests with every task when possible
- Clean code: Naming convention and readable and reusable code
## Key Learning Areas

- Software Development Basics:
  - Introduction to programming concepts and application structure.
- Version Control with GitLab:
  - Managing and version-controlling the project using GitLab.
  - Implementing continuous integration with GitLab CI/CD.
- Database Integration:
  - Connecting the application to a database for persistent data storage.
  - Implementing CRUD operations and managing relational data.
- Web UI Development:
  - Designing and developing a user-friendly web interface for the application.
  - Integrating front-end technologies with back-end services.
- Advanced Programming Concepts:
  - Implementing design patterns and principles.
  - Enhancing the application with advanced features like customizable flight services.
- Route Management and Analysis:
  - Developing algorithms for route finding.
  - Creating a graph-based system for comprehensive flight network analysis.

## Project Goals

- Develop a full-scale application that simulates real-world airline operations.
- Gain hands-on experience in various aspects of software development.
- Foster understanding of both front-end and back-end development processes.

## Deliverables

- A fully functional Airlines management system.
- A comprehensive suite of tests ensuring software reliability and functionality.
- A web-based interface for user interaction with the Airlines system.
  This project aims to provide a holistic learning experience, covering the essentials of software development and preparing interns for real-world challenges in the field.

# In this repository
Here you can find an example configuration files for Roslyn analyzers.
